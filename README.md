# Deploy OpenStack on LXC and Systemd

## TODO

* Set static ips in lxc containers
* ~~Generate LXD profiles~~
* ~~Set a jenkins worker~~
* ~~Split main.yml to match profiles, source, binaries, python, mariadb, etc~~
* execute a pipeline from a netbox webhook

## LXD setup

    ./scripts/setup_lxc.sh

## CI/CD

Cron-like process that builds openstack every 24 hours

Build on commit

If using `supervisord`, jenkins password can be found at `/var/log/supervisor/jenkins-stderr---supervisor-*.log`

## Model

## Deploy OpenStack

    ansible-playbook -i hosts/lxc-hosts.ini openstack-controller-deploy.yml

## Goodies

* atomic clock for NTP <https://ntpserver.wordpress.com/category/atomic-clocks/>
* nginx as replacement for apache
* postgres as replacement for mysql
* ceph
* onos
* prometheus

## Netbox

### Usage

    export NETBOX_ENDPOINT=http://10.166.159.155
    export NETBOX_TOKEN=e60384f68c8de9bf58bf6bd90ddd4425e558a765

    ansible all -i hosts/lxc-hosts.ini -m setup --tree /tmp/facts
    ./scripts/load-netbox-data.py vm-create /tmp/facts/monitoring-0 SITE TENANT

## References

* <https://docs.openstack.org/install-guide/>
* <https://docs.openstack.org/security-guide/databases/database-backend-considerations.html>
* <https://docs.openstack.org/keystone/latest/>
* <https://docs.openstack.org/glance/rocky/install/get-started.html#running-glance-under-python3>
* <https://everythingshouldbevirtual.com/ansible-rabbitmq-cluster/>
* <https://stackoverflow.com/questions/22850546/cant-access-rabbitmq-web-management-interface-after-fresh-install>
* <https://docs.openstack.org/kilo/config-reference/content/list-of-compute-config-options.html>
* <https://docs.openstack.org/neutron/rocky/install/>
* <https://ask.openstack.org/en/question/103199/neutron-linux-bridge-cleanup-fails-on-host-startup/>
* <https://superuser.com/questions/1047891/lxd-containers-and-networking-with-static-ip>
* [Setting up a Dual NIC, LXD Container](https://www.theo-andreou.org/?p=17332)
* [chaos monkey](https://github.com/Netflix/chaosmonkey)
* <https://netbox.readthedocs.io/en/latest/installation/4-ldap/>
* <https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html>
* <https://github.com/elastic/ansible-elasticsearch>
* <https://stgraber.org/2016/10/27/network-management-with-lxd-2-3/>
* <https://help.ubuntu.com/lts/serverguide/lxc.html>
* <https://microk8s.io/>
* <https://stackstorm.com/>
* <https://docs.stackstorm.com/install/index.html>
* <https://github.com/StackStorm/ansible-st2>
