pipeline {
    agent {
        label 'any'
    }
    stages {
        stage ('Pre-Clean'){
            steps {
                cleanWs()
            }
        }
        stage ('Checkout repos'){
            steps {
                dir ('openstack'){
                    git branch: 'dev', poll: false, url: 'https://gitlab.com/memogarcia/openstack.git'
                }
            }
        }
        stage ('Pre-flight'){
            steps {
                dir ('openstack'){
                    ansiblePlaybook colorized: true, disableHostKeyChecking: true, extras: '--check --diff', installation: 'ansible-2.6.5', inventory: 'hosts/lxc-hosts.ini', playbook: 'openstack-controller-deploy.yml'
                }
            }
        }
        stage ('Post-Clean'){
            steps {
                cleanWs()
            }
        }
    }
}
