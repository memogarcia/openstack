#!/usr/bin/env python3

import json
import os
import sys

import click
import pynetbox
from netaddr import IPAddress
from slugify import slugify
from tqdm import tqdm

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


NETBOX_ENDPOINT = os.environ.get("NETBOX_ENDPOINT", default=None)
NETBOX_TOKEN = os.environ.get("NETBOX_TOKEN", default=None)
IP_STATUS = {
    "Active": 1,
    "Reserved": 2,
    "Deprecated": 3
}

if not NETBOX_ENDPOINT:
    raise OSError("environmet var NETBOX_ENDPOINT not set")
if not NETBOX_TOKEN:
    raise OSError("environmet var NETBOX_TOKEN not set")

nb = pynetbox.api(NETBOX_ENDPOINT, NETBOX_TOKEN, ssl_verify=False)


@click.group()
def cli():
    pass


def slug(name):
    return slugify(name)


@cli.command('region-create')
@click.argument('name')
def create_region(name):
    """Create a new region"""
    with open("regions/{0}.json".format(name.lower()), "r") as fd:
        d = json.load(fd)
    data = {
        "name": d["name"],
        "slug": slug(d["name"])
    }
    try:
        nb.dcim.regions.create(**data)
    except Exception as error:
        print(error)


@cli.command('site-create')
@click.argument('region')
@click.argument('name')
def create_site(region, name):
    """Create a new site"""
    with open("sites/{0}.json".format(name.lower()), "r") as fd:
        d = json.load(fd)

    region = nb.dcim.regions.get(name=region)

    data = {
        "name": d["name"],
        "slug": slug(d["name"]),
        "description": d["description"],
        "region": region.id,
        "facility": d["facility"]
    }
    try:
        nb.dcim.sites.create(**data)
    except Exception as error:
        print(error)


@cli.command('vlan-create')
@click.argument('name')
def create_vlans(name):
    """Create a new vLan"""
    with open("sites/{0}.json".format(name.lower()), "r") as fd:
        d = json.load(fd)

    site = nb.dcim.sites.get(name=d["name"])
    for v in tqdm(d["vlans"]):
        vlan = {
            "vid": v["id"],
            "name": v["name"],
            "site": site.id
        }
        try:
            nb.ipam.vlans.create(**vlan)
        except Exception as error:
            print(error)


@cli.command('prefix-create')
@click.argument('name')
def create_prefixes(name):
    """IPv4 or IPv6 network with mask"""
    with open("sites/{0}.json".format(name.lower()), "r") as fd:
        d = json.load(fd)

    site = nb.dcim.sites.get(name=name)
    for v in tqdm(d["vlans"]):
        vlan = nb.ipam.vlans.get(name=v["name"])
        vlan = {
            "prefix": v["prefix"],
            "vlan": vlan.id,
            "is_pool": True,
            "site": site.id
        }
        try:
            nb.ipam.prefixes.create(**vlan)
        except Exception as error:
            print(error)


@cli.command('ips-create')
@click.argument('site')
@click.argument('vlan')
def create_ip_addresses(site, vlan):
    """Create a new IP address on a network"""
    with open("ips/{0}_{1}.json".format(site, vlan), "r") as fd:
        d = json.load(fd)

    for ip in tqdm(d):
        status = 1
        if ip["Status"]:
            if ip["Status"].lower() == "used":
                status = 1
            elif "reserved" in ip["Status"].lower():
                status = 2
            else:
                status = 3
            data = {
                "address": ip["IP"],
                "description": ip["Description"],
                "status": status,
                'custom_fields': {
                    'dns': ip["DNS"],
                    'hostname': ip.get("Hostname", ""),
                    'vlan':  ip["vLan"]
                }
            }
            try:
                nb.ipam.ip_addresses.create(**data)
            except Exception as error:
                print(error)


@cli.command('ips-create-bulk')
def create_ip_addresses_bulk():
    """Load all ips into Netbox"""
    # files = os.listdir("ips")
    files = (f for f in os.listdir("ips/")
             if os.path.isfile(os.path.join("ips/", f)))
    for f in files:
        name = f.split(".")[0]
        site, vlan = name.split("_", 1)

        with open("ips/{0}_{1}.json".format(site, vlan), "r") as fd:
            d = json.load(fd)
        print(f"Exporting {site} {vlan}")
        for ip in tqdm(d):
            status = 1
            if ip["Status"]:
                if ip["Status"].lower() == "used":
                    status = 1
                elif "reserved" in ip["Status"].lower():
                    status = 2
                else:
                    status = 3
                data = {
                    "address": ip["IP"],
                    "description": ip["Description"],
                    "status": status,
                    'custom_fields': {
                        'dns': ip["DNS"],
                        'hostname': ip.get("Hostname", ""),
                        'vlan':  ip["vLan"]
                    }
                }
                try:
                    nb.ipam.ip_addresses.create(**data)
                except Exception as error:
                    print(error)


@cli.command('device-role-create')
@click.argument('name')
def create_device_roles(name):
    """Create a new device role (Virtual Machine, Baremetal Machine, Network Device)
    This option requieres:
        Manufacturer
        Device type
        Site
        Status

    Do this step in the second batch, first ips.
    """
    with open("sites/{0}.json".format(name), "r") as fd:
        d = json.load(fd)

    for device in tqdm(d["device_roles"]):
        data = {
            "name": device["name"],
            "slug": slug(device["name"]),
            "color": device["color"]
        }
        try:
            nb.dcim.device_roles.create(**data)
        except Exception as error:
            print(error)


@cli.command('inventory')
@click.argument('q')
@click.argument('vlan')
def inventory(q, vlan):
    """Output an ansible dynamic inventory for a given regex and a vlan

    ./kpn_netbox inventory {server-regex} {vlan}
    Example: ./kpn_netbox inventory c 306
    """
    try:
        servers = nb.ipam.ip_addresses.filter(q, cf_vlan=vlan)
        for server in servers:
            print(server)
    except Exception as error:
        print(error)


@cli.command('vm-create')
@click.argument('metadata')
@click.argument('site')
@click.argument('tenant')
def create_vm(metadata, site, tenant):
    """Create a VM on Netbox"""
    with open("{0}".format(metadata.lower()), "r") as fd:
        d = json.load(fd)

    d = d["ansible_facts"]

    interfaces = d["ansible_interfaces"]
    interfaces_data = []
    for interface in interfaces:
        if interface == "lo":
            continue
        try:
            i = d["ansible_{0}".format(interface)]
        except KeyError:
            pass
        if not "ipv4" in i.keys():
            continue
        network = {**i["ipv4"]}
        network["interface"] = interface
        interfaces_data.append(network)

    try:
        disk_size = d["ansible_devices"]["sda"]["size"].split(".")[0]
    except Exception:
        disk_size = 0

    vcpus = d["ansible_processor_cores"] * d["ansible_processor_threads_per_core"]

    tenant = nb.tenancy.tenants.get(name=tenant)

    vm = {
            "name": d["ansible_hostname"],
            "cluster": 1,
            "vcpus": vcpus,
            "memory": d["ansible_memtotal_mb"],
            "disk": disk_size,
            "site": site,
            "tenant": tenant.id
    }

    try:
        nb.virtualization.virtual_machines.create(**vm)
    except Exception as error:
        if "already exists" in str(error):
            print("vm already exists")
            sys.exit(0)
    # TODO: m3m0, interfaces can be added again, and ips

    try:
        vm = nb.virtualization.virtual_machines.get(
            name=d["ansible_hostname"]
        )
        for i in interfaces_data:

            vi = nb.virtualization.interfaces.create(
                virtual_machine=vm.id,
                name=i["interface"]
            )

            mask = IPAddress(i["netmask"]).netmask_bits()
            ip = "{0}/{1}".format(i["address"], mask)

            nb.ipam.ip_addresses.create(
                address=ip,
                virtual_machine=vm.id,
                interface=vi.id
            )
    except Exception as error:
        print(error)
    print("Warning, vms don't get created with the right cluster info, please modify it manually")


@cli.command('tenant-create')
@click.argument('tenant')
def create_tenant(tenant):
    """Create a new tenant"""
    with open("tenants/{0}.json".format(tenant.lower()), "r") as fd:
        d = json.load(fd)

    tenant = {
        "name": d["name"],
        "slug": slug(d["name"])
    }

    try:
        nb.tenancy.tenants.create(**tenant)
    except Exception as error:
        print(error)


if __name__ == "__main__":
    cli()
