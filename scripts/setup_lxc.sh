#!/bin/sh

# networking
# sudo lxc network create openstack-mgmt

# containers
sudo lxc launch ubuntu:18.10 openstack-controller-0
sudo lxc launch ubuntu:18.10 openstack-compute-0
sudo lxc launch ubuntu:18.10 ceph-0
sudo lxc launch ubuntu:18.10 jenkins-master-0
sudo lxc launch ubuntu:18.10 jenkins-worker-0
sudo lxc launch ubuntu:18.10 monitoring-0
sudo lxc launch ubuntu:18.10 kubernetes-0 -p multinic

sudo lxc launch images:centos/7/amd64 netbox
sudo lxc launch images:centos/7/amd64 openldap-master
sudo lxc launch images:centos/7/amd64 openldap-slave01 -p multinic
sudo lxc launch images:centos/7/amd64 openldap-slave02 -p multinic

# attach networks to containers

# modify authorized_keys

cat ~/.ssh/id_rsa.pub | sudo lxc file edit openstack-controller-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit openstack-compute-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit jenkins-master-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit jenkins-worker-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit ceph-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit monitoring-0/root/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub | sudo lxc file edit kubernetes-0/root/.ssh/authorized_keys

sudo lxc profile copy default multinic
sudo lxc profile edit multinic