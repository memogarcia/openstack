class Queue:
    """ Queue is an adaptor class to read and write messages to a queue,
    zmq, rabbit, etc.
    """
    def __init__(self, name):
        self.name = name

    @property
    def events(self):
        for x in range(1):
            yield x


class Scheduler:

    def messages(self):
        with Queue("events") as q:
            for e in q.events:
                # json.dumps(e)
                yield e

    def dispatch(self, event):
        """ Should this know about the workers?
        to allocate workloads to specific workers?
        yes

        write to worker queue
        """
        pass

    def log(self, event):
        pass


class Worker:

    def execute(self):
        pass


def scheduler():
    """ Scheduler

    should know about the capabilities of the workers,
    cpus, memory, or something like that to provide flexibility on where to run
    workloads if required

    does this broker need to exists?

    basically this functions just fan out messages
    """
    s = Scheduler()
    for event in s.messages():
        s.log(event)
        s.dispatch(event)


@parallel(processes=auto)
def worker():
    w = Worker()
    with Queue("events") as q:
        for event in q.events():
            w.log(event)
            w.execute(event["command_or_which_name?"])
