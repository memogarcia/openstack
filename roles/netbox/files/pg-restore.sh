#!/bin/bash
## #######################

export PGPASSWORD=netbox

encrypted_backup="$1"
backup="/tmp/netbox_backup.sql"
backup_private_key="/root/.ssh/backup_key.pem"

echo "Decrypting ${encrypted_backup} to /tmp/"
openssl smime -decrypt -in ${encrypted_backup} -binary -inform DEM -inkey ${backup_private_key} | bzcat > ${backup}

# Update netbox database user privileges to allow restore of database
su -l postgres -c 'psql -c "alter role netbox with superuser"'

echo "Restoring database"
psql -h localhost -U netbox -w --set ON_ERROR_STOP=on netbox < ${backup}