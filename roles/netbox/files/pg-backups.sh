#!/bin/bash
## #######################

export PGPASSWORD=netbox

database_name="netbox"
backup_public_key="/root/.ssh/backup_key.pem.pub"

## Location to place backups.
backup_dir="/tmp/db_backups/"
mkdir -p $backup_dir

## Numbers of days you want to keep copies of your databases
number_of_days=7

## String to append to the name of the backup files
backup_date=`date +%Y-%m-%d-%H-%M-%S`
backup_name="${database_name}.${backup_date}.sql.bz2.enc"

echo "Dumping ${database_name} to ${backup_dir}${backup_name}"
pg_dump ${database_name} -h localhost -U netbox -w | bzip2 | openssl smime -encrypt \
    -aes256 -binary -outform DEM \
    -out ${backup_dir}${backup_name} \
    "${backup_public_key}"

## Delete backups older than number_of_days
find ${backup_dir} -type f -prune -mtime \
    +${number_of_days} -exec rm -f {} \;
